package com.codeversant.spwing.config

import com.codeversant.spwing.controller.SendEventAction
import com.codeversant.spwing.controller.ShowControllerAction
import com.codeversant.spwing.event.CloseFrontWindowEvent
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

import javax.swing.*

@Configuration
class MenuConfig{

    @Bean("newControllerAction")
    AbstractAction newControllerAction() {
        return new ShowControllerAction("DemoController")
    }
    @Bean("closeWindowAction")
    AbstractAction closeWindowAction() {
        return new SendEventAction("Close",new CloseFrontWindowEvent())
    }


}
