package com.codeversant.spwing.config

import org.springframework.beans.BeansException
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.stereotype.Component

@Component
class ApplicationContextHolder implements ApplicationContextAware {
    static ApplicationContext applicationContext = null

    @Override
    void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext
    }
    static ApplicationContext getContext() {
        return applicationContext
    }
}
