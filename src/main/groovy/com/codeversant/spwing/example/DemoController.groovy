package com.codeversant.spwing.example

import com.codeversant.spwing.components.ListTableAdapter
import com.codeversant.spwing.controller.WindowController
import com.codeversant.spwing.controller.example.DemoControllerUI
import com.codeversant.spwing.event.UserAddedEvent
import com.codeversant.spwing.model.User
import com.codeversant.spwing.repository.UserRepository
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Scope
import org.springframework.stereotype.Component
import org.springframework.context.event.EventListener

import javax.annotation.PostConstruct
import javax.swing.SwingUtilities
import java.awt.event.ActionEvent

@Scope("prototype")
@Component("DemoController")
class DemoController extends WindowController {
    static Logger logger = Logger.getLogger(DemoController)

    @Autowired
    DemoControllerUI demoControllerUI

    @Autowired
    UserRepository userRepository

    ListTableAdapter<User> listTableAdapter

    @PostConstruct
    void initUi() {
        logger.info "DemoController::initUi"

        createFrame("Demo Frame", demoControllerUI)

        demoControllerUI.button1.addActionListener this.buttonClicked


        listTableAdapter = new ListTableAdapter<User>(columnNames: ["firstName","lastName"],
                                                      columnLabels: ["First Name","Last Name"])
        listTableAdapter.table = demoControllerUI.table1

        refresh(null)
    }
    @EventListener
    def refresh(UserAddedEvent userAddedEvent) {
        logger.info "User Refresh"
        SwingUtilities.invokeLater({
            def users = userRepository.findAll()
            listTableAdapter.rows = users as List
        })
    }
    def buttonClicked = { ActionEvent e ->
        logger.debug "Button Clicked"

        def firstName = demoControllerUI.textFieldOne.text
        def lastName = demoControllerUI.textFieldTwo.text

        User user = new User(firstName: firstName, lastName: lastName)
        userRepository.save(user)
        demoControllerUI.textFieldOne.text = ""
        demoControllerUI.textFieldTwo.text = ""
        sendEvent(new UserAddedEvent())
    }



}
