package com.codeversant.spwing.components

import javax.swing.JTable
import javax.swing.event.ListSelectionEvent
import javax.swing.event.ListSelectionListener
import javax.swing.table.AbstractTableModel
import java.lang.ref.WeakReference


class ListTableAdapter<T> extends AbstractTableModel implements ListSelectionListener {
    WeakReference<JTable> tableWeakReference
    List<T> rows = []
    List<String> columnNames = []
    List<String> columnLabels = []

    ListTableAdapter() {
    }
    void setTable(JTable table) {
        table.model = this
        tableWeakReference = new WeakReference<>(table)
        table.selectionModel.addListSelectionListener(this)
    }
    JTable getTable() {
        return tableWeakReference.get()
    }
    void setRows(List<T> newRows) {
        if(!rows.is(newRows)) {
            rows.clear()
            rows.addAll(newRows)
        }
        fireTableDataChanged()
    }
    @Override
    int getRowCount() {
        return rows.size()
    }

    @Override
    int getColumnCount() {
        return columnNames.size()
    }

    @Override
    String getColumnName(int column) {
        return columnLabels.get(column)
    }

    @Override
    Object getValueAt(int rowIndex, int columnIndex) {
        T item = rows.get(rowIndex)
        def propertyName = columnNames.get(columnIndex)
        def value = null
        if(item.hasProperty(propertyName)) {
            value = item.getAt(propertyName)
        }
        return value
    }

    T getItem(int row) {
        return rows.get(row)
    }
    T getSelectedItem() {
        def table = getTable()
        if(table) {
            int selectedRow = table.getSelectedRow()
            if(selectedRow > -1) {
                return getItem(selectedRow)
            }
        }
        return null
    }
    @Override
    void valueChanged(ListSelectionEvent e) {

    }

}
