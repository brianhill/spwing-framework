package com.codeversant.spwing.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher

import javax.swing.AbstractAction
import java.awt.event.ActionEvent


class SendEventAction extends AbstractAction {
    def event

    @Autowired
    ApplicationEventPublisher applicationEventPublisher

    SendEventAction(String name, Object event) {
        super(name)
        this.event = event
    }

    @Override
    void actionPerformed(ActionEvent e) {
        applicationEventPublisher.publishEvent(event)
    }
}
