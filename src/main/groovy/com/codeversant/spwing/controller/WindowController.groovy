package com.codeversant.spwing.controller

import com.codeversant.spwing.frame.FrameFactory
import com.codeversant.spwing.frame.FrameHolder
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationEventPublisher

import javax.swing.*

class WindowController {
    static Logger logger = Logger.getLogger(WindowController)

    @Autowired
    FrameFactory frameFactory

    @Autowired
    ApplicationEventPublisher applicationEventPublisher

    FrameHolder frameHolder
    PanelController rootPaneController



    def createFrame(String name,PanelController paneController) {
        rootPaneController = paneController
        frameHolder = frameFactory.createFrame(name);
        JPanel jpanel = rootPaneController.mainPanel
        frameHolder.setContent(jpanel)
        frameHolder.pack()
        hide()
        return frameHolder
    }

    void show() {
        frameHolder.show()
    }
    void hide() {
        frameHolder.hide()
    }

    void close() {
        frameHolder.contentPane.clear()
        frameHolder.close()
        rootPaneController = null
    }
    void sendEvent(Object event) {
        SwingUtilities.invokeLater({
            applicationEventPublisher.publishEvent(event)
        })
    }
    /*@EventListener
    void handleCloseWindow(CloseFrontWindowEvent frontWindowEvent) {
        if(frameHolder.isFrontWindow()) {
            frameHolder.close()
        }
    }*/

}
