package com.codeversant.spwing.controller

import com.codeversant.spwing.config.ApplicationContextHolder
import org.springframework.context.ApplicationContext

import javax.swing.AbstractAction
import java.awt.event.ActionEvent


class ShowControllerAction extends AbstractAction {

    String controllerBean

    ShowControllerAction(String beanName) {
        super("New")
        controllerBean = beanName
    }
    @Override
    void actionPerformed(ActionEvent e) {
        def controller = ApplicationContextHolder.context.getBean(controllerBean) as WindowController
        controller.show()
    }
}
