package com.codeversant.spwing.controller;


import javax.swing.*;

public interface PanelController {
    JPanel getMainPanel();
}
