package com.codeversant.spwing.frame

import org.apache.log4j.Logger

import javax.swing.*
import javax.swing.event.InternalFrameEvent
import javax.swing.event.InternalFrameListener
import java.awt.*

class JInternalFrameHolder implements FrameHolder, InternalFrameListener {
    static Logger logger = Logger.getLogger(JInternalFrameHolder)

    JInternalFrame jInternalFrame


    def createFrame(String name) {
        jInternalFrame = new JInternalFrame(name, true, true, true, true)
        jInternalFrame.setDefaultCloseOperation(JInternalFrame.DO_NOTHING_ON_CLOSE)
        jInternalFrame.addInternalFrameListener(this)
        jInternalFrame.putClientProperty(this,"frameHolder")
        return jInternalFrame
    }

    @Override
    Container getContentPane() {
        return jInternalFrame?.contentPane
    }
    @Override
    void setContent(Component component) {
        contentPane?.add(component)
    }
    @Override
    void hide() {
        jInternalFrame?.setVisible(false)
    }

    @Override
    void show() {
        jInternalFrame?.setVisible(true)
    }

    @Override
    void pack() {
        jInternalFrame?.pack()
    }
    void close() {
        logger.info "Closing: ${jInternalFrame}"
        jInternalFrame.dispose()
        //jInternalFrame.dispatchEvent(new InternalFrameEvent(jInternalFrame, InternalFrameEvent.INTERNAL_FRAME_CLOSING));
    }


    @Override
    boolean isFrontWindow() {

        def selected = jInternalFrame.isFocusCycleRoot()
        logger.info "Is isFocusCycleRoot: ${selected}"
        return selected

    }

    @Override
    void internalFrameOpened(InternalFrameEvent e) {

    }

    @Override
    void internalFrameClosing(InternalFrameEvent e) {
        logger.info "internalFrameClosing"
        close()
    }

    @Override
    void internalFrameClosed(InternalFrameEvent e) {
        logger.info "internalFrameClosed"
        //jInternalFrame = null
    }

    @Override
    void internalFrameIconified(InternalFrameEvent e) {

    }

    @Override
    void internalFrameDeiconified(InternalFrameEvent e) {

    }

    @Override
    void internalFrameActivated(InternalFrameEvent e) {
    }

    @Override
    void internalFrameDeactivated(InternalFrameEvent e) {
    }
    @Override
    void setMenuBar(JMenuBar jMenuBar) {
        jInternalFrame.setJMenuBar(jMenuBar)
    }


}
