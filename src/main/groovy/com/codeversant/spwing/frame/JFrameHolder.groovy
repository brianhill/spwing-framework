package com.codeversant.spwing.frame

import javax.swing.JFrame
import javax.swing.JMenuBar
import java.awt.Component
import java.awt.Container
import java.awt.KeyboardFocusManager
import java.awt.event.WindowEvent
import java.awt.event.WindowListener



class JFrameHolder implements FrameHolder, WindowListener {
    JFrame frame

    def createFrame(String name) {
        frame = new JFrame(name)
        frame.addWindowListener(this)
        return frame
    }

    @Override
    Container getContentPane() {
        return frame?.contentPane
    }

    @Override
    void setContent(Component component) {
        contentPane?.add(component)
    }

    @Override
    void hide() {
        frame?.setVisible(false)
    }

    @Override
    void show() {
        frame?.setVisible(true)
    }

    @Override
    void pack() {
        frame?.pack()
    }

    @Override
    void close() {
        if(frame) {
            frame.dispose()
        }
    }

    @Override
    void windowOpened(WindowEvent e) {

    }

    @Override
    void windowClosing(WindowEvent e) {

    }

    @Override
    void windowClosed(WindowEvent e) {
        frame = null
    }

    @Override
    void windowIconified(WindowEvent e) {

    }

    @Override
    void windowDeiconified(WindowEvent e) {

    }

    @Override
    void windowActivated(WindowEvent e) {

    }

    @Override
    void windowDeactivated(WindowEvent e) {

    }

    boolean isFrontWindow() {
        KeyboardFocusManager focusManager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        return frame == focusManager.getActiveWindow()
    }

    @Override
    void setMenuBar(JMenuBar jMenuBar) {
        frame.setJMenuBar(jMenuBar)
    }
}
