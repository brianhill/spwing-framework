package com.codeversant.spwing.frame

import javax.swing.JDialog
import javax.swing.JFrame
import javax.swing.JInternalFrame
import javax.swing.JMenuBar
import javax.swing.RootPaneContainer
import java.awt.Component
import java.awt.Container


interface FrameHolder {
    Container getContentPane()
    void setContent(Component component)
    void hide()
    void show()
    void pack()
    void close()
    boolean isFrontWindow()

    void setMenuBar(JMenuBar jMenuBar)
}
