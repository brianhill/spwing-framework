package com.codeversant.spwing.frame

import javax.swing.JDialog
import javax.swing.JMenuBar
import java.awt.Component
import java.awt.Container


class JDialogFrameHolder implements FrameHolder {
    JDialog jDialog

    @Override
    Container getContentPane() {
        return jDialog?.contentPane
    }

    @Override
    void setContent(Component component) {
        jDialog.contentPane.add(component)
    }

    @Override
    void hide() {
        jDialog?.setVisible(false)
    }

    @Override
    void show() {
        jDialog?.setVisible(true)

    }

    @Override
    void pack() {
        jDialog?.pack()
    }

    @Override
    void close() {
        jDialog.dispose()
        jDialog = null
    }
    boolean isFrontWindow() {
        return jDialog.isFocusOwner()
    }
    @Override
    void setMenuBar(JMenuBar jMenuBar) {
        jDialog.setJMenuBar(jMenuBar)
    }
}
