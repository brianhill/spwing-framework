package com.codeversant.spwing.frame

import com.codeversant.spwing.event.CloseFrontWindowEvent
import org.apache.log4j.Logger
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct
import javax.swing.*
import java.awt.*

@Component
class FrameFactory {
    static Logger logger = Logger.getLogger(FrameFactory)

    JDesktopPane desktop
    JFrame mdiFrame

    boolean useMdi = true

    @PostConstruct
    def init() {
        if(useMdi) {
            mdiFrame = new JFrame("Application");
            desktop = new JDesktopPane();
            desktop.setPreferredSize(new Dimension(1024, 768));
            mdiFrame.setContentPane(desktop)
            mdiFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            //mdiFrame.setJMenuBar(theApp.getMainMenu());
            mdiFrame.pack();
            mdiFrame.setVisible(true);
        } else {

        }
    }

    FrameHolder createFrame(String frameName) {
        if(useMdi) {
            return createInternalFrame(frameName)
        } else {
            return createJframe(frameName)
        }
}
    FrameHolder createJframe(String frameName) {
        FrameHolder frameHolder = new JFrameHolder()
        frameHolder.createFrame(frameName)
        return frameHolder
    }

    FrameHolder createInternalFrame(String frameName) {
        FrameHolder frameHolder = new JInternalFrameHolder()
        def frame = frameHolder.createFrame(frameName)
        frame.putClientProperty("frameHolder",frameHolder)
        def component = desktop.add(frameHolder.jInternalFrame)
        logger.debug "Desktop Add returned: ${component}"
        return frameHolder
}

    @EventListener
    void closeSelectedFrame(CloseFrontWindowEvent closeFrontWindowEvent) {
        def jInternalFrame = desktop.getSelectedFrame()
        def frameHolder = jInternalFrame.getClientProperty("frameHolder") as FrameHolder
        frameHolder?.close()
        //jInternalFrame.dispatchEvent(new InternalFrameEvent(jInternalFrame, InternalFrameEvent.INTERNAL_FRAME_CLOSING));

    }

}
