package com.codeversant.spwing.model

import groovy.transform.Canonical

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Canonical
@Entity
class User {
    @GeneratedValue
    @Id
    Long id

    String firstName

    String lastName

}
