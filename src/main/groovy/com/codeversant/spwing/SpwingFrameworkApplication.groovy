package com.codeversant.spwing

import com.codeversant.spwing.frame.FrameFactory
import com.codeversant.spwing.util.SpwingUtilities
import org.apache.log4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware

import javax.annotation.PostConstruct
import javax.swing.*

@SpringBootApplication
public class SpwingFrameworkApplication implements ApplicationContextAware {
    static Logger logger = Logger.getLogger(SpwingFrameworkApplication)

    ApplicationContext applicationContext


    @Autowired
    FrameFactory frameFactory

    @Autowired
    @Qualifier("newControllerAction")
    AbstractAction newControllerAction

    @Autowired
    @Qualifier("closeWindowAction")
    AbstractAction closeWindowAction

    @PostConstruct
    def showController() {
        def jmenuBar = new JMenuBar()
        def fileMenu = new JMenu("File")

        fileMenu.add(newControllerAction)
        fileMenu.add(closeWindowAction)
        jmenuBar.add(fileMenu)

        frameFactory.mdiFrame?.setJMenuBar(jmenuBar)

        SwingUtilities.invokeLater({
            newControllerAction.actionPerformed(null)
        })


    }

	public static void main(String[] args) {
        UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        logger.info "Jar Directory: ${SpwingUtilities.getJarDirectory(this)}"

        SpwingApplication.run(SpwingFrameworkApplication.class, args)

	}



}
