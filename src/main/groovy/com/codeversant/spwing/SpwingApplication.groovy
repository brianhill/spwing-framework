package com.codeversant.spwing

import org.springframework.boot.builder.SpringApplicationBuilder


class SpwingApplication  {

    static void run(Class clazz,String[] args) {
        new SpringApplicationBuilder(clazz)
                .headless(false)
                .web(false)
                .run(args);
    }
}
