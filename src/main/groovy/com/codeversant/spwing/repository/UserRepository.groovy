package com.codeversant.spwing.repository

import com.codeversant.spwing.model.User
import org.springframework.data.repository.CrudRepository


interface UserRepository extends CrudRepository<User,Long> {
    List<User> findByLastNameLike(String name)

}