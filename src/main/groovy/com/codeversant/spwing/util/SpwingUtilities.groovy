package com.codeversant.spwing.util

import java.security.CodeSource

/**
 * Created by brianhill on 2/18/17.
 */
class SpwingUtilities {
    static String getJarDirectory(Class clazz) {
        CodeSource codeSource = clazz.getProtectionDomain().getCodeSource();
        File jarFile = new File(codeSource.getLocation().toURI().getPath());
        String jarDir = jarFile.getParentFile().getPath();
        return jarDir
    }
}
