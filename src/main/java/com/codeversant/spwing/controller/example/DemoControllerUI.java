package com.codeversant.spwing.controller.example;

import com.codeversant.spwing.controller.PanelController;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.swing.*;

@Component
@Scope("prototype")
public class DemoControllerUI implements PanelController {
    public JPanel mainPanel;
    public JTextField textFieldTwo;
    public JTextField textFieldOne;
    public JButton button1;
    public JTable table1;


    @Override
    public JPanel getMainPanel() {
        return mainPanel;
    }
}
